// Теоретический вопрос
//
// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// \(обратный слэш) — символ экранирования.
// 1. являэтся началом любого спецсимвола
// 2. используется если необходимо вставить в строку кавычку или обратный слеш
//
//
//
// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//
//     Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией
// (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


function createNewUser() {
    this.firstName = prompt('Enter your first name ','');
    while (this.firstName === ''){
        this.firstName = prompt('Please enter your first name again ','');
    }

    this.lastName = prompt('Enter your last name','');
    while (this.lastName === ''){
        this.lastName = prompt('Please enter your last name again ','');
    }

    this.getLogin = function(){
        return  this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    };

    this.getPassword = function(){
        return  this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase()+this.birthday.getFullYear();
    };

    this.birthday = new Date(prompt("Enter your birthday"));

    this.getAge = function () {
        let today = new Date();
        let age = today.getFullYear() - this.birthday.getFullYear();
        let m = today.getMonth() - this.birthday.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < this.birthday.getDate())) {
            age--;
        }
        return age;
    };
}

let newUserObj = new createNewUser();
console.log(`Your login is ${newUserObj.getLogin()}`);
console.log(`Your age is ${newUserObj.getAge()}`);
console.log(`Your password is ${newUserObj.getPassword()}`);

